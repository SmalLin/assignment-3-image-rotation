//
// Created by 24374 on 2023/3/14.
//
#include "bmp.h"
#include "file.h"
#include "img.h"

#include "stdbool.h"
#include "stdint.h"
#include "stdio.h"
#include "stdlib.h"

enum {
    BMP_PIXEL_SIZE = sizeof(struct pixel),
    BMP_HEADER_SIZE = sizeof(struct bmp_header)
};

uint32_t calPaddding(uint32_t width) {
    return (4 - (width * 3) % 4) % 4;
}
    struct bmp_header create_header(struct image *image) {
        const uint32_t image_size = calImgSize(image->width, image->height);
        const uint32_t file_size = calFileSize(image_size);

        return (struct bmp_header) {
                .bfType = BMP_SIGNATURE,
                .bfileSize = file_size,
                .bfReserved = 0,
                .bOffBits = BMP_HEADER_SIZE,
                .biSize = BMP_SIZE,
                .biWidth = image->width,
                .biHeight = image->height,
                .biPlanes = 1,
                .biBitCount = PIXEL_SIZE,
                .biCompression = COMPRESSION,
                .biSizeImage = image_size,
                .biXPelsPerMeter = 0,
                .biYPelsPerMeter = 0,
                .biClrUsed = 0,
                .biClrImportant = 0
        };
    }
uint32_t calImgSize(uint32_t width, uint32_t height) {
    return (width + calPaddding(width)) * BMP_PIXEL_SIZE * height;
}

uint32_t calFileSize(uint32_t img_size) {
    return (img_size + BMP_HEADER_SIZE);
}

enum read_status bmp_in(FILE *in, struct image *image) {
    struct bmp_header header;

    if (fread(&header, BMP_HEADER_SIZE, 1, in) != 1) {
        return ReadError;
    }

    if (header.bfType != BMP_SIGNATURE) {
        return ReadInvalidSignature;
    }

    if (header.biBitCount != PIXEL_SIZE) {
        return ReadInvalidBits;
    }

    if (header.biSize <= 0) {
        return ReadInvalidHeader;
    }

    if (header.biWidth <= 0 || header.biHeight <= 0) {
        return ReadInvalidHeader;
    }

    *image = img_create(header.biWidth, header.biHeight);

    uint32_t padding = calPaddding((uint32_t) image->width);

    for (uint64_t i = 0; i < image->height; i++) {
        void *pointerStart = image->data + image->width * i;

        if (fread(pointerStart, BMP_PIXEL_SIZE, image->width, in) != image->width) {
            img_destroy(*image);

            return ReadError;
        }

        if (fseek(in, padding, SEEK_CUR)) {
            img_destroy(*image);

            return ReadError;
        }
    }

    return ReadCorrect;
}

enum write_status bmp_out(FILE *out, struct image *image) {
    struct bmp_header out_header;

    uint32_t padding = calPaddding((uint32_t) image->width);

    out_header = create_header(image);

    if (fwrite(&out_header, BMP_HEADER_SIZE, 1, out) == 0) {
        return WriteError;
    }

    for (uint64_t i = 0; i < image->height; i++) {
        void *pointerStart = (image->data + image->width * i);

        if (fwrite(pointerStart, BMP_PIXEL_SIZE, image->width, out) == 0) {
            img_destroy(*image);

            return WriteError;
        }
        uint8_t garb[3] = {0};

        if ((padding != 0) && (!fwrite(&garb, padding, 1, out))) {
            img_destroy(*image);

            return WriteError;
        }
    }

    return WriteSuccess;
}
