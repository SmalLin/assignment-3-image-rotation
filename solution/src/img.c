//
// Created by 24374 on 2023/3/14.
//
#include "bmp.h"
#include "file.h"
#include "img.h"

#include "stdbool.h"
#include "stdint.h"
#include "stdio.h"
#include "stdlib.h"

struct image img_create(uint32_t width, uint32_t height) {
    struct image img;

    img.width = width;
    img.height = height;
    img.data = (struct pixel *) malloc(sizeof(struct pixel) * width * height);
    if (img.data == NULL) {
        printf("Error allocating memory for image data\n");

        return img;
    }

    return img;
}

void img_destroy(struct image img) {
    free(img.data);
    img.height = 0;
    img.width = 0;
}

struct image img_rotate(struct image const source) {
    struct image r_image = img_create(source.height, source.width);

    for (uint64_t i = 0; i < r_image.height; i++) {
        for (uint64_t j = 0; j < r_image.width; j++) {
            r_image.data[i * r_image.width + j] =
                    source.data[(source.height - j - 1) * source.width + i];
        }
    }

    return r_image;
}
