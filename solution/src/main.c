
#include "bmp.h"
#include "file.h"
#include "img.h"

#include "stdbool.h"
#include "stdint.h"
#include "stdio.h"
#include "stdlib.h"

struct image old_img;
struct image rotated_image;

enum state file_in_status;
enum state file_out_status;

FILE **file_in;
FILE **file_out;

static void check_attribute(int argc) {
    if (argc != 3) {
        printf("Error: Arguments Error!");
        exit(1);
    }
}

static void get_files_status(char **argv) {
    file_in_status = open_file(file_in, argv[1], "rb");
    if (file_in_status == Work) {
        printf("Log: Input file open\n");
    } else {
        printf("Error: Input file open error!\n");
        free_file(file_in, file_out);

        exit(1);
    }

    file_in_status = open_file(file_out, argv[2], "wb");
    if (file_in_status == Work) {
        printf("Log: Output file open\n");
    } else {
        printf("Error: File open error!\n");
        free_file(file_in, file_out);

        exit(1);
    }


    enum read_status file_read_status = bmp_in(*file_in, &old_img);

    if(file_read_status == ReadError){
        printf("Error: Read error!\n");
    } else if (file_read_status == ReadInvalidBits){
        printf("Error: Read invalid bits!\n");
    } else if (file_read_status == ReadInvalidHeader){
        printf("Error: Read invalid header!\n");
    } else if(file_read_status == ReadInvalidSignature){
        printf("Error: Read invalid signature!\n");
    }

    if (file_read_status != ReadCorrect) {
        free_file(file_in, file_out);

        if (fclose(*file_in) != 0) {
            printf("Error: Closing input file failed!\n");
        }

        if (fclose(*file_out) != 0) {
            printf("Error: Closing output file failed!\n");
        }

        exit(1);
    }
}

static void file_save_and_destroy(struct image old, struct image rotated) {
    img_destroy(old);

    enum write_status file_write_status = bmp_out(*file_out, &rotated);
    if (file_write_status == WriteSuccess) {
        printf("Log: bmp writen");
    } else {
        img_destroy(rotated);
        free_file(file_in, file_out);
        printf("Error: bmp write failed!");

        exit(1);
    }

    img_destroy(rotated);

    file_out_status = close_file(*file_in);
    if (file_out_status == Work) {
        printf("Log: Input file success!\n");
    } else {
        printf("Error: File close failed!");
        free_file(file_in, file_out);

        exit(1);
    }

    file_out_status = close_file(*file_out);

    if (file_out_status == Work) {
        printf("Log: Output file success!\n");
    } else {
        printf("Error: File close failed!");
        free_file(file_in, file_out);

        exit(1);
    }

    free_file(file_in, file_out);
}

int main(int argc, char **argv) {
    check_attribute(argc);

    file_in = malloc(sizeof(FILE *));
    file_out = malloc(sizeof(FILE *));

    get_files_status(argv);

    rotated_image = img_rotate(old_img);
    printf("Log: Image rotated success!\n");

    file_save_and_destroy(old_img, rotated_image);

    return 0;
}
