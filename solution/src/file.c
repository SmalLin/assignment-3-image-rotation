//
// Created by 24374 on 2023/3/14.
//

#include "bmp.h"
#include "file.h"
#include "img.h"

#include "stdbool.h"
#include "stdint.h"
#include "stdio.h"
#include "stdlib.h"

enum state open_file(FILE **file, char const *path, char const *mode) {
    *file = fopen(path, mode);

    return *file == NULL ? OpenError : Work;
}
void free_file(FILE **file1, FILE **file2) {
    free(file1);
    free(file2);
}

enum state close_file(FILE *file) {
    if (fclose(file) == 0) {
        return Work;
    } else {
        return CloseError;
    }
}
