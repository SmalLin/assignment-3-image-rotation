//
// Created by 24374 on 2023/3/14.
//

#ifndef ASSIGNMENT_3_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_3_IMAGE_ROTATION_BMP_H

#define BMP_SIGNATURE 0x4d42
#define PIXEL_SIZE 24
#define BMP_SIZE 40
#define COMPRESSION 0

#include "file.h"
#include "img.h"

#include "stdbool.h"
#include "stdint.h"
#include "stdio.h"
#include "stdlib.h"

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

#pragma pack(pop)

enum read_status {
    ReadCorrect = 0,
    ReadInvalidSignature,
    ReadInvalidBits,
    ReadInvalidHeader,
    ReadError
};

enum write_status {
    WriteSuccess = 0,
    WriteError
};

struct bmp_header create_header(struct image *image);

enum read_status bmp_in(FILE *in, struct image *image);

enum write_status bmp_out(FILE *out, struct image *image);

uint32_t calImgSize(uint32_t width, uint32_t height);

uint32_t calFileSize(uint32_t img_size);

uint32_t calPaddding(uint32_t width);




#endif //ASSIGNMENT_3_IMAGE_ROTATION_BMP_H
