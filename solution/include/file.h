//
// Created by 24374 on 2023/3/14.
//

#ifndef ASSIGNMENT_3_IMAGE_ROTATION_FILE_H
#define ASSIGNMENT_3_IMAGE_ROTATION_FILE_H

#include "bmp.h"
#include "img.h"

#include "stdbool.h"
#include "stdint.h"
#include "stdio.h"
#include "stdlib.h"

enum state {
    Work = 0,
    OpenError,
    CloseError
};

void free_file(FILE **file1, FILE **file2);

enum state open_file(FILE **file, char const *path, char const *mode);

enum state close_file(FILE *file);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_FILE_H
